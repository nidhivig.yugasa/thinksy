 <!-- Sidenav -->
 <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
  <div class="scrollbar-inner">
    <!-- Brand -->
    <div class="sidenav-header  align-items-center">
      <a class="navbar-brand" href="javascript:void(0)">
          <span class="text-info border-info border-bottom"><strong>Thinksy</strong> Dashboard</span>
        {{-- <img src="../assets/img/brand/blue.png" class="navbar-brand-img" alt="..."> --}}
      </a>
    </div>
    <div class="navbar-inner">
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Nav items -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="">
            <i class="fas fa-tv"></i>
              <span class="nav-link-text">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="">
              <i class="fa fa-users" aria-hidden="true"></i>
              <span class="nav-link-text">User Management</span>
              </a>
          </li>
          </ul>
      </div>
    </div>
  </div>
</nav>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Thinksy: @yield('title')</title>
  @yield('styles')
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">

  <link rel="stylesheet" href="{{asset('css/nucleo.css')}}" type="text/css">
  <link rel="stylesheet" href="{{asset('css/custom.css')}}" type="text/css">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
  <link rel="stylesheet" href="{{ asset('css/argon.css?v=1.2.0') }}" type="text/css">

</head>

<body>

  @include('admin.layout.left_menu')

  <div class="main-content" id="panel">

    @include('admin.layout.header')
    @yield('content')

  </div>

  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('js/js.cookie.js') }}"></script>
  <script src="{{ asset('js/sidebarmenu.js') }}"></script>
  <script src="{{ asset('js/jquery.scrollbar.min.js') }}"></script>
  <script src="{{ asset('js/jquery-scrollLock.min.js') }}"></script>
  <script src="{{ asset('js/argon.js?v=1.2.0') }}"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  
  @yield('scripts')
</body>

</html>

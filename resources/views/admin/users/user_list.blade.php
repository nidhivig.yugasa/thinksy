@extends('admin.layout.layout')
@section('title', 'Users')
@section('styles')
<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap4.min.css') }}">
<style>
    .font-10
    {
        font-size: 10px !important;
    }
</style>
@stop
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
                <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                    </ol>
                </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="" class="btn btn-sm btn-neutral">Add new</a>
          </div>
        </div>
      </div>
    </div>
</div>

@php
    $list = '';
    if (count($users) > 0) {
        $usersss = Auth::guard('admins')->user();
        $name = $usersss->name;
        foreach ($users as $key => $user) {

            $dlt = '';
            $btn = '';
            if ($user->is_verified == 1) {
                $status = '<span class="badge badge-dot mr-4">
                                <i class="bg-success"></i>
                                <span class="status">Verified</span>
                            </span>';
                if ($user->is_blocked == 0) {
                    $block = '<span class="badge badge-danger font-10">Block</span>';
                    $btn = '<a class="dropdown-item block" href="'.route('admin-userBlock',['user_id' => $user->id, 'block' => $user->is_blocked]).'" data-b="unblock" data-t="'.csrf_token().'"><i class="fad fa-user text-primary"></i> Unblock</a>';
                }
                else {
                    $block = '<span class="badge badge-primary font-10">Unblock</span>';
                    $btn = '<a class="dropdown-item block" href="'.route('admin-userBlock',['user_id' => $user->id, 'block' => $user->is_blocked]).'" data-b="block" data-t="'.csrf_token().'"><i class="fad fa-user-slash text-info"></i> Block</a>';
                }
            }
            else {
                $status = '<span class="badge badge-dot mr-4">
                                <i class="bg-warning"></i>
                                <span class="status">Pending</span>
                            </span>';
                $dlt = '<a class="dropdown-item delete" href="'.route('admin-userDelete',['user_id' => Crypt::encryptString($user->id)]).'" data-t="'.csrf_token().'"><i class="fal fa-trash-alt text-danger"></i> Delete</a>';
                $block = '<span class="badge badge-danger font-10">Block</span>';
            }

            $profile = 'https://readyrefrigeration.ca/sites/default/files/styles/headshot/adaptive-image/public/nobody.jpg';
            if (!empty($user->profile)) {
                $profile = $user->profile;
            }


            if ($user->is_deleted == 0) {
                $list .= '<tr>
                            <th scope="row">
                                <div class="media align-items-center">
                                <span class="avatar rounded-circle mr-3">
                                    <img alt="'.ucfirst($user->name).'" src="'.$profile.'" class="h-100">
                                </span>
                                <div class="media-body">
                                    <span class="name mb-0 text-sm">'.ucfirst($user->name).'</span>
                                </div>
                                </div>
                            </th>
                            <td class="budget">
                                '.$user->email.'
                            </td>
                            <td>
                                <span>'.$user->mobile.'</span>
                            </td>
                            <td>
                                <span>'.\Carbon\Carbon::parse($user->dob)->format('d-m-Y').'</span>
                            </td>
                            <td class="text-center">
                                <span>'.\Carbon\Carbon::parse($user->dob)->age.'</span>
                            </td>
                            <td>
                                '.$status.'
                            </td>
                            <td>
                            '.$block.'
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="'.route('admin-user-form',['user_id' => Crypt::encryptString($user->id),'type' => Crypt::encryptString('edit')]).'"><i class="fal fa-user-edit text-info"></i> Edit</a>
                                        '.$dlt.'
                                        '.$btn.'
                                    </div>
                                </div>
                            </td>
                        </tr>';
            }


        }
    }
@endphp


  <div class="container-fluid mt--6">
    @if ($message = \Illuminate\Support\Facades\Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="m-0"><strong>{{$message}}</strong></h5>
        </div>
    @endif

    <div class="row">
        <div class="col">
          <div class="card">
            <div class="card-header border-0">
              <h3 class="mb-0">@yield('title') List</h3>
            </div>
            <div class="table-responsive mb-3">
              <table id="example" class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort text-center" data-sort="name">Name</th>
                    <th scope="col" class="sort text-center" data-sort="budget">Email</th>
                    <th scope="col" class="sort text-center" data-sort="status">Mobile</th>
                    <th scope="col" class="text-center">DOB</th>
                    <th scope="col">Age <small class="text-muted">(In year)</small></th>
                    <th scope="col" class="sort text-center" data-sort="completion">Status</th>
                    {{-- @php
                        $user = Auth::guard('admins')->user();
                        $name = $user->name;
                    @endphp
                    @if ($name == 'Developer') --}}
                    <th scope="col" class="sort" data-sort="completion">Block</th>
                    <th scope="col">Action</th>
                    {{-- @endif --}}
                  </tr>
                </thead>
                <tbody class="list">
                    {!! $list !!}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
  </div>
@stop
@section('scripts')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            // "ordering": desc
        });
    });

    $(document).on('click','.block',function(e) {
        e.preventDefault();
        var _this = $(this);
        var url = _this.attr('href');
        var block = _this.data('b');
        var type = 'POST';
        alertStatus(block,_this,url,type)
    });

    $(document).on('click','.delete',function(e) {
        e.preventDefault();
        var _this = $(this);
        var url = _this.attr('href');
        var type = 'GET';
        alertStatus('delete',_this,url,type)
    });

</script>
@stop
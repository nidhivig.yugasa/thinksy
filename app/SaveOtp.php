<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class SaveOtp extends Model
{
   protected $table = 'otp';

   protected $fillable = [
       'email', 'otp'
   ];

   public $timestamps = false;
}
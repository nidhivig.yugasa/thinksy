<?php

namespace App\Http\Controllers\UserApi;

use App\Http\Controllers\Controller;
use App\User;
use App\AppUser;
use App\SaveOtp;
use App\Mail\newMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */

    /**
     * Get a JWT token via given credentials.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */

    public function login(Request $request)
    {   
        $message = [
            'email.required' => 'Email is required!',
            'email.email'    => 'Email is invalid!',
            'email.exists'   => 'Email not found!',
        ];

        $validate = Validator::make($request->all(), [
            'email'    => 'required|email|exists:app_users',
            'password' => 'required|min:8',
        ], $message);

        if ($validate->fails()) {
            return response()->json(["status" => 'failed', 'message' => $validate->errors()->first()], 401);
        }

        $credentials = $request->only('email', 'password');

        if (!$token = Auth::guard('api')->attempt($credentials)) {
            return response()->json(['status'=>'failed', 'message' => 'Credential Invalid'], 401);
        }
        $authToken =  $token;
        unset($token);
        $users = User::find(Auth::guard('api')->user()->id);
        $users->token = $authToken;
        $users->save();

        $user         = User::find(Auth::guard('api')->user()->id);

        if ($user->is_verified == 0) {
            $data = array('name'=>ucfirst($user->name));
            $email = $user->email;
            $otp = $this->generateOtp();
            Mail::send([], $data, function($message) use ($email,$otp) {
                $message->to($email)->subject
                ('OTP for verification')->setBody('Verification code: '.$otp);
                $message->from(env('MAIL_USERNAME'),env('APP_NAME'));
            });
            return response()->json([
                'status'  => 'success',
                'message' => 'You are not verified. please check your email id for verification.',
                'user' => $user
            ]);
        }

        unset($user->user_profile);

        $user->access_token = $authToken;
        $user->token_type   = 'Bearer';

        return response()->json(['message' => 'You have been successfully logged in.', 'status' => 'success', 'user' => $user]);
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return JsonResponse
     */

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return $this->sendResponse('Successfully logged out', null);
    }

    public function register(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:app_users',
            'password' => 'required|string|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'dob'     => 'required_with:vendor',
            'phone' => 'required|numeric|digits:10|unique:app_users',
        ], ['password.regex' => 'Your password must be a minimum of 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.']);

        if ($validator->fails()) {
            return $this->sendError('Invalid Input', 422, $validator->errors()->first());
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
          
        // if(!SaveOtp::where(['email' => $request->email, 'otp' => $request->json()->get('otp')])->exists()){
        //     return response()->json(['status' => 'fail', 'message' => 'Incorrect OTP'], 200);
        // }

        $user = AppUser::create($input);
        $success['token'] =  $user->createToken('authToken')->accessToken;
        $success['name'] =   $user->name;
        $success['email'] =  $user->email;
        $success['phone'] =  $user->phone;

        $email = $request->json()->get('email');
        $randomotp = rand(100000, 999999);
        $msg = "Your one time password (OTP) is ".$randomotp." for Thinksy App.";
        $message = "$msg";
        
        SaveOtp::create([
            'email' => $email,
            'otp' => $randomotp
        ]);
        $details = [
            'title' => 'OTP',
            'body' => $message
        ];
        try{
            Mail::to($email)->send(new newMail($details));
            // return ['mail'=>"done"];
            return $this->sendResponse('OTP sent successfully!', $success);

        }catch(Exception $e){
            $exception=$e;
        }
        // return $this->sendResponse('OTP sent successfully!', $success);
    
}
public function verifiedOtp(Request $request)
    {
        $message = [
            'otp.required' => 'OTP is required!',
            'otp.digits' => 'OTP should be in 6 digits!',
            'otp.exists'   => 'OTP does not exist!',
        ];

        $validate = Validator::make($request->all(), [
            'email'   => 'required|email|exists:otp',
            'otp'   => 'required|digits:6|exists:otp',
        ], $message);

        if ($validate->fails()) {
            $error = $validate->errors()->first();
            return response()->json(['status' => 'failed', 'message' => $error]);
        }

        $verify_otp = SaveOtp::where(['email' => $request->email,'otp' => $request->otp])->first();
        $verify_email = AppUser::where(['email' => $request->email])->first();

        if ($verify_otp && $verify_email)
        {
            $verify_email->is_verified = 1;
            $verify_email->save();
            return response()->json(['status' => 'success', 'message' => 'OTP Verified.']);
        }

        return response()->json(['status' => 'failed', 'message' => 'OTP not verified.']);

    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */

    public function sendResponse($message, $result)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }
    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */

    public function sendError($error, $code = 404, $errorMessages = [])
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }

    public function changePassword(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'confirm_password' => ['same:new_password'],
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors()->first(), 422);
        }

        if (strcmp($request->current_password, $request->new_password) == 0) {
            //Current password and new password are same
            return $this->sendError("New Password cannot be same as your current password. Please choose a different password.", 422);
        }

        AppUser::find(auth()->user()->id)->update(['password' => bcrypt($request->new_password)]);

        return $this->sendResponse('Password change successfully.', null);
    }
}
